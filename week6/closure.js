// closure example
function makeAdder(x) {
  return function(y) {
    return x + y;
  };
}

const add5 = makeAdder(5);
const add10 = makeAdder(10);

console.log(add5(7));
console.log(add10(9));

// parctical examplea
function wait(message) {
  setTimeout(function timer() {
    console.log(message);
  }, 1000);
}

wait("Hello, closure!");

// for loop issue
for (var i = 1; i <= 5; i++) {
  setTimeout(function timer() {
    console.log(i);
  }, i * 1000);
}

// module

function calculator() {
  let counter = 0;

  function add(value) {
    counter += value;
  }

  function getResult() {
    return counter;
  }

  return {
    add: add,
    getResult: getResult
  };
}

const calculator1 = calculator();
calculator1.add(4);
calculator1.add(5);
console.log(calculator1.getResult());
const calculator2 = calculator();
calculator2.add(10);
calculator2.add(9);
console.log(calculator2.getResult());
