function winnerMessage() {
  return "The winner is: ";
}

function lotteryWinner(name, callback) {
  const message = callback();

  return message + name;
}

console.log(lotteryWinner("Smith", winnerMessage));
