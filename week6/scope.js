// how many scopes can you spot?
function func1(a) {
    function func2(b) {
        console.log(b);
    }

    function func3(c) {
        console.log(c);
    }

    console.log(func2(2));
    console.log(func3(3));
}

func1(1);

// global scope
function foo(a) {
  // foo scope
  var b = a * 2;

  function bar(c) {
    // bar scope
    console.log(a, b, c);
  }

  bar(b * 3);
}

foo(2);
