// Function no arguments
function sumNoArgs() {
  return 1 + 2;
}

console.log(sumNoArgs());

// Classic function declaration
function sum(a, b) {
  return a + b;
}

console.log(sum(1, 4));

// Function expression
const expressionSumFunc = function(a, b) {
  return a + b;
};

console.log(expressionSumFunc(1, 2));

// function (method) in class
class TestFunctions {
  sum(a, b) {
    return a + b;
  }
}

console.log(new TestFunctions().sum(1, 2));

// function as object prop
const testObject = {
  sum: function(a, b) {
    return a + b;
  }
};

console.log(testObject.sum(1, 2));
