var obj = JSON.parse(`
{ 
    "name": "Michalis", 
    "age": 29,
    "address":{
      "street":"1 Main St",
      "city": "Montreal"
    },
    "interests":["movies", "biking"]
    }
`);

console.log(obj);

console.log(JSON.stringify(obj));

let request = new XMLHttpRequest();
request.open(
  "GET",
  "http://dummy.restapiexample.com/api/v1/employee/148899",
  true
);
request.onload = function() {
  // Convert JSON data to an object
  document.getElementById("result").innerHTML = JSON.stringify(
    JSON.parse(this.response),
    undefined,
    4
  );
  console.log(this.response);
};

request.send();
