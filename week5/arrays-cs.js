// create  an array
// brackets
const arrayWithBrackets = [1, 2, 5, 8];

console.log(arrayWithBrackets);

// new Array
const arrayWithNew = new Array(1, 2, 5, 8);

console.log(arrayWithNew);

// array length
let arrayLength = new Array(20);
console.log(arrayLength);
arrayLength = [1];
arrayLength[20] = 10;
console.log(arrayLength);
arrayLength.length = 1;
console.log(arrayLength);

// loop through array
const myArray = [3, 1, 4];

myArray.forEach(function(value) {
  console.log("Value: " + value);
});

// index of
console.log(myArray.indexOf(1));
console.log(myArray.indexOf(9));

// slice
const arrayCopied = myArray.slice();
console.log(arrayCopied);
console.log(arrayCopied == myArray);

// splice
console.log(arrayCopied.splice(5, 1));
console.log(arrayCopied.splice(1, 2));
console.log(arrayCopied.splice(1, 2, 5, 8), arrayCopied);

// push
arrayCopied.push(1, 5, 6);
console.log(arrayCopied);

// pop
arrayCopied.pop();
console.log(arrayCopied);

// join
console.log(arrayCopied.join(" | "));

// concat
const concatResult = [1, 2, 3].concat([4, 5, 6], [7, 8, 9]);
const arrayOne = [1, 2, 3];
const concatResult2 = [...arrayOne, ...[4, 5, 6], ...[7, 8, 9]];
console.log(concatResult);
console.log(concatResult2);

// map
const mapResult = myArray.map(function(value, index, array) {
  return value * 10;
});

console.log(myArray, mapResult);

// find
const findResult = ["a", "b", "a"].find(function(value) {
  return value === "a";
});

console.log(findResult);

const sortResult = [10, 1, 3, 2, 90, 0].sort(function(a, b) {
  return a - b;
});

const sortResult2 = [10, 1, 3, 2, 90, 0].sort(function(a, b) {
  if (a < b) {
    return -1;
  }
  if (b < a) {
    return 1;
  }

  return 0;
});

console.log(sortResult);
console.log(sortResult2);

// filter
const filterResult = [1, 11, 12, 3, 6].filter(function(value) {
  return value % 2 === 0;
});
console.log(filterResult);

// every
const everyResult = [1, 2].every(function(value) {
  return value % 2 === 0;
});

console.log(everyResult);

// some
const someResult = [1, 2].some(function(value) {
  return value % 2 === 0;
});

console.log(someResult);

// reduce
const sumResult = [1, 4, 7, 10].reduce(function(acc, value) {
  return acc + value;
}, 0);

console.log(sumResult);
