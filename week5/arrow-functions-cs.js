// arrow function
let myArrowFunction = (a, b) => {
  return a + b;
};

console.log(myArrowFunction(1, 2));

// single line
myArrowFunction = (a, b) => a + b;
const singleLineObject = (a, b) => ({
  a,
  b,
  sum: a + b
});
console.log(myArrowFunction(1, 2));
console.log(singleLineObject(1, 2));

// no params
const noParams = () => {
  return "test";
};

console.log(noParams());

// single param
const singleParam = value => {
  return value;
};

console.log(singleParam("test"));
